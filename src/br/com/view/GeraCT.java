package br.com.view;

import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.*;

public class GeraCT implements ActionListener {

	private JFrame frm;
	private JPanel pan;
	private JButton btnGerar;
	private JButton btnAdd;
	private JButton btnSave;
	private JTextField txtNomeCT;
	private JTextArea txtDesc;
	private JTextArea txtPre;
	private JTextArea txtPreview;
	private JTextField txtString;
	private JTextField txtString2;
	private JTextField txtString3;
	private JTextField txtString4;
	private JTextField txtComent;
	private JComboBox cmbComando;
	private JComboBox cmbTipo;
	private JLabel lblValor;

	public void frame() throws IOException {

		frm = new JFrame("RSI - Gerador de Casos de Testes - Beta");

		// Setando configura��o do panel
		pan = new JPanel();
		pan.setLayout(null);

		JLabel lblNomeCT = new JLabel("Nome do Caso de Teste: ");
		lblNomeCT.setBounds(11, 2, 200, 20);
		pan.add(lblNomeCT);

		txtNomeCT = new JTextField();
		txtNomeCT.setBounds(11, 22, 500, 20);
		pan.add(txtNomeCT);

		JLabel lblDesc = new JLabel("Descri��o: ");
		lblDesc.setBounds(11, 45, 200, 20);
		pan.add(lblDesc);

		txtDesc = new JTextArea();
		txtDesc.setBounds(11, 65, 500, 60);
		pan.add(txtDesc);

		JLabel lblPre = new JLabel("Pr� Requisitos: ");
		lblPre.setBounds(11, 130, 200, 20);
		pan.add(lblPre);

		txtPre = new JTextArea();
		txtPre.setBounds(11, 150, 500, 60);
		pan.add(txtPre);

		JLabel lblComando = new JLabel("Comando: ");
		lblComando.setBounds(11, 220, 200, 20);
		pan.add(lblComando);

		String[] comandos = { "CLICAR", "VALIDAR", "INSERIR", "DESLIZAR", "PAUSA" };
		cmbComando = new JComboBox(comandos);
		cmbComando.addActionListener(this);
		cmbComando.setActionCommand("Comando");
		cmbComando.setBounds(11, 240, 100, 20);
		pan.add(cmbComando);
		cmbComando.setEnabled(false);

		JLabel lblTipo = new JLabel("Tipo: ");
		lblTipo.setBounds(140, 220, 200, 20);
		pan.add(lblTipo);

		cmbTipo = new JComboBox();
		cmbTipo.addActionListener(this);
		cmbTipo.setActionCommand("Tipo");
		cmbTipo.setBounds(140, 240, 100, 20);
		pan.add(cmbTipo);
		cmbTipo.setEnabled(false);

		// Setando bot�o
		btnAdd = new JButton("Add");
		btnAdd.setBounds(457, 240, 55, 20);
		btnAdd.setActionCommand("Add");
		btnAdd.addActionListener(this);

		btnSave = new JButton("Salvar");
		btnSave.setBounds(356, 240, 65, 20);
		btnSave.setActionCommand("Save");
		pan.add(btnSave);
		btnSave.addActionListener(this);

		JLabel lblComent = new JLabel("Coment�rio: ");
		lblComent.setBounds(11, 260, 200, 20);
		pan.add(lblComent);

		txtComent = new JTextField();
		txtComent.setBounds(11, 280, 410, 20);
		pan.add(txtComent);
		txtComent.setEnabled(false);

		JLabel lblPreview = new JLabel("Preview: ");
		lblPreview.setBounds(11, 330, 200, 20);
		pan.add(lblPreview);

		txtPreview = new JTextArea();
		txtPreview.setBounds(11, 350, 500, 170);
		pan.add(txtPreview);
		txtPreview.setEditable(false);

		// Setando bot�o
		btnGerar = new JButton("Gerar Caso de Teste");
		btnGerar.setBounds(7, 530, 150, 30);
		btnGerar.setActionCommand("Gerar");
		pan.add(btnGerar);
		btnGerar.addActionListener(this);

		// Defini��es finais do JFrame
		frm.setSize(530, 600);
		ImageIcon img = new ImageIcon(System.getProperty("user.dir")+"\\rsi.png");
		frm.setIconImage(img.getImage());
		frm.setVisible(true);
		frm.setContentPane(pan);
		frm.setLocationRelativeTo(null);
		frm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frm.setResizable(false);
	}

	public static void main(String[] args) throws Exception {
		UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		GeraCT gerador = new GeraCT();
		gerador.frame();
	}

	@Override
	public void actionPerformed(ActionEvent e) {

		if (e.getActionCommand().equalsIgnoreCase("Comando")) {
			if (cmbComando.getSelectedItem().equals("CLICAR")) {
				String[] tipos = { "POSICIONAL", "ID", "NAME", "XPATH" };
				pan.remove(cmbTipo);
				cmbTipo = new JComboBox(tipos);
				cmbTipo.addActionListener(this);
				cmbTipo.setActionCommand("Tipo");
				cmbTipo.setBounds(140, 240, 100, 20);
				pan.add(cmbTipo);
			} else if (cmbComando.getSelectedItem().equals("VALIDAR")) {
				String[] tipos = { "NAME" };
				pan.remove(cmbTipo);
				cmbTipo = new JComboBox(tipos);
				cmbTipo.addActionListener(this);
				cmbTipo.setActionCommand("Tipo");
				cmbTipo.setBounds(140, 240, 100, 20);
				pan.add(cmbTipo);
			} else if (cmbComando.getSelectedItem().equals("INSERIR")) {
				String[] tipos = { "TEXTO" };
				pan.remove(cmbTipo);
				cmbTipo = new JComboBox(tipos);
				cmbTipo.addActionListener(this);
				cmbTipo.setActionCommand("Tipo");
				cmbTipo.setBounds(140, 240, 100, 20);
				pan.add(cmbTipo);
			} else if (cmbComando.getSelectedItem().equals("DESLIZAR")) {
				String[] tipos = { "POSICIONAL" };
				pan.remove(cmbTipo);
				cmbTipo = new JComboBox(tipos);
				cmbTipo.addActionListener(this);
				cmbTipo.setActionCommand("Tipo");
				cmbTipo.setBounds(140, 240, 100, 20);
				pan.add(cmbTipo);
			} else if (cmbComando.getSelectedItem().equals("PAUSA")) {
				String[] tipos = { "MILISEGUNDOS" };
				pan.remove(cmbTipo);
				cmbTipo = new JComboBox(tipos);
				cmbTipo.addActionListener(this);
				cmbTipo.setActionCommand("Tipo");
				cmbTipo.setBounds(140, 240, 100, 20);
				pan.add(cmbTipo);
			}
			pan.repaint();
		} else if (e.getActionCommand().equals("Tipo")) {

			try {
				pan.remove(txtString);
				pan.remove(txtString2);
				pan.remove(txtString3);
				pan.remove(txtString4);
				pan.remove(lblValor);
			} catch (Exception ex) {
			}

			if (cmbComando.getSelectedItem().equals("CLICAR") && cmbTipo.getSelectedItem().equals("POSICIONAL")) {
				txtString = new JTextField();
				txtString.setBounds(270, 240, 40, 20);
				pan.add(txtString);
				txtString2 = new JTextField();
				txtString2.setBounds(330, 240, 40, 20);
				pan.add(txtString2);
			} else if (cmbComando.getSelectedItem().equals("VALIDAR") && cmbTipo.getSelectedItem().equals("NAME")) {
				txtString = new JTextField();
				txtString.setBounds(270, 240, 40, 20);
				pan.add(txtString);
				txtString2 = new JTextField();
				txtString2.setBounds(330, 240, 40, 20);
				pan.add(txtString2);
			} else if (cmbTipo.getSelectedItem().equals("TEXTO")) {
				txtString = new JTextField();
				txtString.setBounds(270, 240, 150, 20);
				pan.add(txtString);
			} else if (cmbTipo.getSelectedItem().equals("XPATH")) {
				txtString = new JTextField();
				txtString.setBounds(270, 240, 150, 20);
				pan.add(txtString);
			} else if (cmbTipo.getSelectedItem().equals("ID")) {
				txtString = new JTextField();
				txtString.setBounds(270, 240, 150, 20);
				pan.add(txtString);
			} else if (cmbTipo.getSelectedItem().equals("NAME")) {
				txtString = new JTextField();
				txtString.setBounds(270, 240, 150, 20);
				pan.add(txtString);
			} else if (cmbTipo.getSelectedItem().equals("MILISEGUNDOS")) {
				txtString = new JTextField();
				txtString.setBounds(270, 240, 150, 20);
				pan.add(txtString);
			} else if (cmbComando.getSelectedItem().equals("DESLIZAR")
					&& cmbTipo.getSelectedItem().equals("POSICIONAL")) {
				txtString = new JTextField();
				txtString.setBounds(270, 240, 30, 20);
				pan.add(txtString);
				txtString2 = new JTextField();
				txtString2.setBounds(310, 240, 30, 20);
				pan.add(txtString2);
				txtString3 = new JTextField();
				txtString3.setBounds(350, 240, 30, 20);
				pan.add(txtString3);
				txtString4 = new JTextField();
				txtString4.setBounds(390, 240, 30, 20);
				pan.add(txtString4);
			}

			lblValor = new JLabel("Valor: ");
			lblValor.setBounds(270, 220, 200, 20);
			pan.add(lblValor);
			frm.repaint();
		} else if (e.getActionCommand().equals("Add")) {
			if (cmbComando.getSelectedItem().equals("CLICAR") && cmbTipo.getSelectedItem().equals("POSICIONAL")) {
				txtPreview.append("\r\n" + "CLICAR;POSICIONAL");
				txtPreview.append(";" + txtString.getText().trim());
				txtPreview.append(";" + txtString2.getText().trim());
				txtPreview.append(";" + txtComent.getText().trim());
			} else if (cmbComando.getSelectedItem().equals("CLICAR") && cmbTipo.getSelectedItem().equals("ID")) {
				txtPreview.append("\r\n" + "CLICAR;ID");
				txtPreview.append(";" + txtString.getText().trim());
				txtPreview.append(";" + txtComent.getText().trim());
			} else if (cmbComando.getSelectedItem().equals("CLICAR") && cmbTipo.getSelectedItem().equals("XPATH")) {
				txtPreview.append("\r\n" + "CLICAR;XPATH");
				txtPreview.append(";" + txtString.getText().trim());
				txtPreview.append(";" + txtComent.getText().trim());
			} else if (cmbComando.getSelectedItem().equals("CLICAR") && cmbTipo.getSelectedItem().equals("NAME")) {
				txtPreview.append("\r\n" + "CLICAR;NAME");
				txtPreview.append(";" + txtString.getText().trim());
				txtPreview.append(";" + txtComent.getText().trim());
			} else if (cmbComando.getSelectedItem().equals("INSERIR") && cmbTipo.getSelectedItem().equals("TEXTO")) {
						txtPreview.append("\r\n" + "INSERIR;TEXTO");
						txtPreview.append(";" + txtString.getText().trim());
						txtPreview.append(";" + txtComent.getText().trim());
			} else if (cmbComando.getSelectedItem().equals("DESLIZAR")
					&& cmbTipo.getSelectedItem().equals("POSICIONAL")) {
				txtPreview.append("\r\n" + "DESLIZAR;POSICIONAL");
				txtPreview.append(";" + txtString.getText().trim());
				txtPreview.append(";" + txtString2.getText().trim());
				txtPreview.append(";" + txtString3.getText().trim());
				txtPreview.append(";" + txtString4.getText().trim());
				txtPreview.append(";" + txtComent.getText().trim());
			} else if (cmbComando.getSelectedItem().equals("VALIDAR") && cmbTipo.getSelectedItem().equals("NAME")) {
				txtPreview.append("\r\n" + "VALIDAR;NAME");
				txtPreview.append(";" + txtString.getText().trim());
				txtPreview.append(";" + txtString2.getText().trim());
				txtPreview.append(";" + txtComent.getText().trim());
			} else if (cmbComando.getSelectedItem().equals("PAUSA")) {
				txtPreview.append("\r\n" + "PAUSA");
				txtPreview.append(";" + txtString.getText().trim());
			}
			frm.repaint();
		} else if (e.getActionCommand().equals("Save")) {
			txtPreview.append("NOMECT;" + txtNomeCT.getText().trim());
			txtPreview.append("\r\n" + "DESCRICAO;" + txtDesc.getText().trim());
			txtPreview.append("\r\n" + "PRE;" + txtPre.getText().trim());
			pan.add(btnAdd);
			pan.remove(btnSave);
			cmbComando.setEnabled(true);
			cmbTipo.setEnabled(true);
			txtComent.setEnabled(true);
			txtNomeCT.setEnabled(false);
			txtDesc.setEnabled(false);
			txtPre.setEnabled(false);
			frm.repaint();
			cmbComando.setSelectedIndex(0);
		} else if (e.getActionCommand().equals("Gerar")) {
			txtPreview.append("\r\n" + "FIM;");
			frm.repaint();
			try {

				JFileChooser salvar = new JFileChooser();

				if (salvar.showSaveDialog(null) == JFileChooser.APPROVE_OPTION) {
					FileWriter file = new FileWriter(salvar.getSelectedFile() + ".txt");
					BufferedWriter writer = new BufferedWriter(file);// cria
																		// caneta
					writer.write(txtPreview.getText());

					writer.close();
					file.close();
					txtPreview.setText("");
				}
			} catch (Exception e2) {

			}
		}
	}

}
